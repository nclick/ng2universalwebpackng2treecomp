import { Route } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';

export const Routes: Route[] = [
    {
        path: 'dashboard',
        component: DashboardComponent,
        pathMatch: 'full',
    },
    {
        path: '',
        component: DashboardComponent
    }
];