import { NgModule } from '@angular/core';
import { UniversalModule } from 'angular2-universal';
import { APP_BASE_HREF } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AppComponent } from './components/app/app.component';
import { Routes } from './app-routes';
import { DashboardModule } from './components/dashboard/dashboard.module';

@NgModule({
    imports: [
        UniversalModule,
        RouterModule.forRoot(Routes),
        DashboardModule,
    ],
    declarations: [AppComponent],
    bootstrap: [AppComponent]
})

export class AppModule { }
