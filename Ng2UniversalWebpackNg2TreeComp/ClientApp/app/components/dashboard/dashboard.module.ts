﻿import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { TreeModule } from 'angular2-tree-component';


@NgModule({
    imports: [CommonModule, TreeModule],
    //declarations: [HomeComponent],
    //exports: [HomeComponent],
    //providers: [NameListService]
    //imports: [FormsModule],
    declarations: [DashboardComponent]
})
export class DashboardModule { }
